# Teaching Robots Behaviors Ontology

The ontology for teaching behaviors (and more), and all the common C++
code for manipulating NAOqi's knowledge easily in that context.
Name of the Qi Project and of the library: `teachingontology`.

This is distributed under the [BSD license](LICENSE).

## General

The Semantic Agent uses Knowledge in a particular way, specified by this
document. In this document, we will enumerate classes of resources through
examples written using the [Turtle representation](https://www.w3.org/TR/turtle/).
Variable parts in the examples are expressed between curly brackets,
as in `{variable_name}`.

We may use notions from the [RDF Schema](https://www.w3.org/TR/rdf-schema/),
so you must always keep in mind:

```turtle
@prefix rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#> .
@prefix rdfs: <http://www.w3.org/2000/01/rdf-schema#> .
```

Also, keep in mind that when `{locale}` is mentioned, it stands for the RDF
representation of locales, that is the typical ISO string `"{language}_{region}"`
(region being optional).

In this project, knowledge remains mostly local, until we are ready to exchange
behaviors across robots. To explicit that, we use a specific URI root:

```turtle
@prefix qi: <qiknowledge://> .
```

## Behaviors

The Semantic Agent relies on behaviors. Every notion surrounding
behaviors should be put in a specific namespace:

```turtle
@prefix behavior qi:behavior# .
```

Behaviors can be learned by composition, from an animation, or predefined
with code. Each implementation has its own logic and is to be interpreted
specifically. But discussing and reasoning about behaviors must be possible
regardless that implementation.

By "behavior" here, we usually mean a class of behavior. Each behavior has
a unique URI. You can build it with according to this template:
`behavior:behavior-{uid}` (NOT UUID). It will be referred to simply as `{behavior}`
later on. But what will make it recognizable programmatically is the following
triple:

```turtle
{behavior} rdf:type behavior:behavior
```

SPARQL to find all behaviors, regardless their implementation:

```sparql
SELECT ?behavior
WHERE {
  ?behavior rdf:type behavior:behavior
}
```

A behavior has an implementation. Each implementation is unique, and may
inherit a specific class, so that to interpret them correctly. For example,
for a behavior implemented as an animation:

```turtle
{behavior} behavior:implemented_by {implementation}
{implementation} rdf:type behavior:animation
```

SPARQL to find all animation behaviors:

```sparql
SELECT ?behavior
WHERE {
  ?behavior rdf:type behavior:behavior
  ?behavior behavior:implemented_by ?implementation
  ?implementation rdf:type behavior:animation
}
```

## Animation behaviors

The behavior implementation with animations simply associates animation data
with an implementation. URI suggestion: `behavior:animation-{uid}`.

```turtle
{animation} rdf:type behavior:animation
{animation} behavior:animation_content "{animation_data_1}"
{animation} behavior:animation_content "{animation_data_2}"
```

Animation data is actually the content of .pmt or a .anim file inlined in
the database. If there are several contents associated to the same animation,
it should be interpreted as to be played simultaneously.

## Simple behavior composition

This behavior implementation is a sequence of other behaviors. URI
suggestion: `behavior:composition-{uid}`

```turtle
{composition} rdf:type behavior:composition
{composition} behavior:composed_of {child_behavior_1}
{composition} behavior:composed_of {child_behavior_2}
{child_behavior_2} behavior:consecutive_to {child_behavior_1}
```

To resolve the behavior to execute, an algorithm would look at the
implementation of each child behavior.

Note that `behavior:consecutive_to` expresses consecutivity, and may be reused
for episode description too.

SPARQL to find all child behaviors of a behavior:

```sparql
SELECT ?child
WHERE {
  {behavior} behavior:implemented_by ?composition
  ?composition rdf:type behavior:composition
  ?composition behavior:composed_of ?child
}
```

SPARQL to find the starting points of a composition:

```sparql
SELECT ?child
WHERE {
  {composition} behavior:composed_of ?child
  FILTER NOT EXISTS {
    ?child behavior:consecutive_to ?previous
  }
}
```

It is possible to extend the means of composition. See
[this proposition](simultaneity-in-composition)
for representing simultaneity in the knowledge.

## Simultaneity in composition

TODO: prove it is useful before using this; it is possible that everything
should be considered simultaneous by default, *à la* blockly.

To express a behavior that does two things at the same time, it is like
for [consecutivity](simple-behavior-composition), but with a different
predicate, `behavior:concurrent_with`:

```turtle
{composition} rdf:type behavior:composition
{composition} behavior:composed_of {child_behavior_1}
{composition} behavior:composed_of {child_behavior_2}
{child_behavior_2} behavior:concurrent_with {child_behavior_1}
```

## Natural language behavior

It is also possible to express behaviors as natural language that can be
interpreted by the semantic engine.

```turtle
{behavior} behavior:implemented_by {implementation}
{implementation} rdf:type behavior:natural_language
{implementation} behavior:natural_language_content "{natural_language}"@{locale}
```

The semantic engine might still produce errors when executing the behavior;
this piece of knowledge does not bring a strong guarantee that the semantic
engine knows the behavior associated to the natural language content.

## Natural language labeling of behaviors

In the standard, anything with a `rdfs:label` is meant to be expressible in
natural language. Behaviors can be expressed that way too, and the semantic
engine can take advantage that labeling to identify them.

```turtle
{behavior} rdfs:label "{label}"@{locale}
```

Associated with the [behaviors implemented in natural language](#natural-language-behavior),
the semantic engine can collect all the behavior compositions in natural
language:

```sparql
SELECT ?behavior_label ?recipe_description
WHERE {
  ?behavior rdfs:label ?behavior_label
  ?behavior behavior:implemented_by ?impl
  ?impl rdf:type behavior:natural_language
  ?impl behavior:natural_language_content ?recipe_description
}
```

You can also extract the recipe in natural language from a behavior composition
using only `rdfs:label` of child behaviors, but it is not obvious to write
in a single query.

## Parametrized behaviors

Many behaviors may be parametrized. We will express it as follows:

```turtle
{behavior} behavior:requires_parameter {parameter_1}
{behavior} behavior:accepts_parameter {parameter_2}
```

Parameters can then have other kind of relations, so that to bind them with
[semantic slots](TODO), with [Qi SDK actions](#qi-sdk-actions), etc, or to
provide them types or other type of constraints that may be useful for
reasoning regardless the implementation of the behavior.

Behaviors like "to say" can be specialized by fixing parameters they accept,
like in "to say hello". It results in a separate behavior, bound to the
original behavior, but with an explicit specification of a parameter:

```turtle
{behavior_2} behavior:binds {behavior_1}
{behavior_2} behavior:uses_parameter {behavior_2_parameter_a}
{behavior_2_parameter_a} behavior:binds {behavior_1_parameter_a}
{behavior_2_parameter_a} behavior:uses_value {value}
```

The predicate `behavior:binds` may be used for various kind of indirections,
and implies, by transitivity, that the same parameters are required or
accepted by the binding. That is to say, the following:

```turtle
{behavior_1} behavior:requires_parameter {behavior_1_parameter_a}
{behavior_2} behavior:binds {behavior_1}
```

Implies:

```turtle
{behavior_2} behavior:requires_parameter {behavior_1_parameter_a}
{behavior_2_parameter_a} behavior:binds {behavior_1_parameter_a}
```

The latter can theoretically omitted, but for the sake of the SPARQL
writer's sanity, it should be explicited in the knowledge, so that he
or she can write:

```sparql
SELECT ?parameter
WHERE {
  {behavior} behavior:requires_parameter ?parameter
}
```

Otherwise, he or she should write at least:

```sparql
SELECT ?parameter
WHERE {
  IF(NOT EXISTS({behavior} behavior:binds ?behavior))
  {behavior} behavior:requires_parameter ?parameter
} UNION {
  {behavior} behavior:binds ?behavior
  ?behavior behavior:requires_parameter ?parameter
}
```

If not worse, because the binding can be recurrent!

## Qi SDK actions

Qi SDK actions can be referred to because they are unique. They are
systematically associated with one or several factories, and they accept,
if not require, arguments. The Qi SDK provides IDL and .json files that
can be used to gather the information. Knowing that:

- the Qi SDK ensures the argument names are consistent across overloads.
- the name of the service matches to a unique name of a type in the Qi SDK.
- the Qi SDK provides a binding between action types and their factories.
- the names of the action types are unique in the Qi SDK.
- the IDL contains all the information about the returned action type, and
of each argument expected in the factory.

We can assume that the name of the action type is sufficient to find its
factories and identify its arguments. Therefore we suppose the
following URI template contains sufficient information:
`qisdk://action/{action_type}#{argument_name}`.

And we only need to expose the following in the knowledge:

```turtle
{implementation} rdf:type behavior:qisdk_action
{implementation} behavior:requires_qisdk_argument {argument_1}
{implementation} behavior:accepts_qisdk_argument {argument_2}
```

For example, for the say action:

```turtle
@prefix action: <qisdk://action/>
action:Say behavior:requires_qisdk_argument action:Say#phrase
action:Say behavior:accepts_qisdk_argument action:Say#locale
action:Say behavior:accepts_qisdk_argument action:Say#bodyLanguageOption
```

Now to translate it into the generic interface:

```turtle
{behavior} behavior:requires_parameter {parameter_1}
{behavior} behavior:accepts_parameter {parameter_2}
{behavior} behavior:implemented_by {action}
{parameter_1} behavior:binds_qisdk_argument {argument_1}
{parameter_2} behavior:binds_qisdk_argument {argument_2}
```

Note how `behavior:binds_qisdk_argument` is specific to the case of
Qi SDK actions. Though it resembles the predicate used for
[binding parameters in compositions](composition-with-parameters),
it must remain different so that clients can interpret it differently.
Here for example, a behavior parameter known in the knowledge must be
converted into a type of the Qi SDK so that the action factory accepts it.

In practice, to figure out which action argument corresponds to a
behavior parameter:

```sparql
SELECT ?argument
WHERE {
  {parameter} behavior:binds_qisdk_argument ?argument
}
```

Qi SDK actions can be specialized directly, for example to make a
behavior where the robot burps by saying "burp":

```turtle
{behavior} rdfs:label "to burp"@en_us
{behavior} behavior:implemented_by action:Say
{behavior} behavior:uses_parameter {parameter}
{parameter} behavior:binds_qisdk_argument action:Say#phrase
{parameter} behavior:uses_value "burp"@en_us
```

To resolve this information into a method call, we would perform
a query retrieving pairs of argument and value:

```sparql
SELECT ?argument ?value
WHERE {
  {behavior} behavior:implemented_by {action}
  {behavior} behavior:uses_parameter ?parameter
  ?parameter behavior:uses_value ?value
  ?parameter behavior:binds_qisdk_argument ?argument
}
```

Then, depending on the value type in memory, and the argument type
in the IDL, the proper conversions should take place.

## Composition with parameters

Composed behaviors may require parameters too, but parameters are not
meaningful for all child behaviors. It is important that the parameters
are associated clearly between the parent and the child behaviors.
This association is symbolized by the predicate `behavior:binds`.

```turtle
{behavior} behavior:implemented_by {composition}
{composition} rdf:type behavior:composition

{composition} behavior:composed_of {child_behavior_1}
{child_behavior_1} behavior:requires_parameter {child_behavior_1_param_a}

{composition} behavior:composed_of {child_behavior_2}
{child_behavior_2} behavior:accepts_parameter {child_behavior_2_param_a}

{child_behavior_2} behavior:consecutive_to {child_behavior_1}

{behavior} behavior:requires_parameter {behavior_param_1}
{behavior_param_1} behavior:binds {child_behavior_1_param_a}

{behavior} behavior:accepts_parameter {behavior_param_2}
{behavior_param_2} behavior:binds {child_behavior_2_param_b}
```

Note that a same parameter from the parent can also bind two separate
parameters, from separate child behaviors:

```turtle
{behavior} behavior:implemented_by {composition}
(...)
{behavior} behavior:requires_parameter {behavior_param_1}
{behavior_param_1} behavior:binds {child_behavior_1_param_a}
{behavior_param_1} behavior:binds {child_behavior_2_param_b}
```

Also, child behaviors may be specializations of existing behaviors. This
would be expressed by the `behavior:uses_parameter` and `behavior:uses_value`
predicates. For example, the turtle for "to greet is to raise the right arm
and to say hello" would be:

```turtle
{greet_behavior} rdfs:label "to greet"@en_us
{greet_behavior} behavior:implemented_by {composition}
{composition} rdf:type behavior:composition

{composition} behavior:composed_of {child_behavior_1}
{child_behavior_1} behavior:binds {raise_right_arm_behavior}
{raise_right_arm_behavior} rdfs:label "to raise the right arm"@en_us

{composition} behavior:composed_of {child_behavior_2}
{child_behavior_2} behavior:binds {say_behavior}
{say_behavior} rdfs:label "to say"@en_us
{child_behavior_2} behavior:uses_parameter {child_behavior_2_param_a}
{child_behavior_2_param_a} behavior:uses_value "hello"@en_us

{child_behavior_2} behavior:consecutive_to {child_behavior_1}
```

## Parametrized labeling in natural language with semantic frames

Using the predicate `rdfs:label` we can represent any kind of resource
in natural language. But parametrized behaviors are more complex, because
the parameter is yet to be filled: it is a slot, corresponding in the
*frame semantics* theory to what is known as a *semantic slot*.

A *semantic frame* corresponds to a situation, here, performing an action,
regardless the lexic used to describe it. A good example is
[FrameNet](https://framenet.icsi.berkeley.edu/fndrupal/frameIndex),
which lists many of them.

The semantic engine is able to recognize similar concepts by comparing
lemmas, so that various natural language forms may match the same
semantic frame.

We propose to use some kind of template in natural language,
that would provide a structure that can be matched by the
semantic engine, as well as identify the slots it accepts, like in:
"to say #phrase", or "to look at #target".

It can then be associated to the semantic frame and slot entities:

```turtle
@prefix semantic: qi:semantic/
{semantic_frame} rdf:type semantic:frame
{semantic_frame} semantic:natural_language_template {phrase_with_slot}
{semantic_frame} semantic:slot {semantic_slot}
```

The name of the semantic frame should be unique, and may match
semantic engine's concepts. For "to look at #target":

```turtle
semantic:look_at rdf:type semantic:frame
semantic:look_at semantic:natural_language_template "to look at #target"
semantic:look_at semantic:slot semantic:look_at#target
```

Then behaviors can bind semantic frames:

```turtle
{semantic_frame} rdf:type semantic:frame
{semantic_frame} semantic:natural_language_template {phrase_with_slot}
{semantic_frame} semantic:slot {semantic_slot}
{behavior} behavior:expressed_by {semantic_frame}
{behavior_param_1} behavior:binds_semantic_slot {semantic_slot}
```

Note that the same pattern as for implementation was used. We could
technically extend the means of expression, and express things using
movement, or non-lexical utterances.

## Social Agents and Humans

By "Humans", we mean physical presences that resemble human beings,
as proposed in the Qi SDK API. These objects are temporary, situational,
and should not be remembered in the knowledge as is. However, the
individual that have been around (as a Human), may be
identified and remembered as a *Social Agent*.

We introduce the agent namespace:

```turtle
@prefix agent qi:agent# .
```

We introduce social agent nodes, which can be built around the following URI
template: `agent:social_agent-{uid}`. It may be associated with :
- a `rdfs:label`
- a path to face picture,
- events
- another social agent where the two are colocalized, which means there are in
the same area, in proximity with each other.

It must be of
`rdf:type` `agent:social_agent`:

```turtle
{social_agent}
    rdf:type agent:social_agent;
    rdfs:label "Victor"@fr-fr
    agent:face_picture {picture}
    agent:colocalized_with {social_agent}
```

`{picture}` should be a path to a storage that can be actually resolved.

`{social_agent}` should be the node of another *social agent* that actually exists in the knowledge.

## Robot

The robot's physical body may be represented by its own node. It might be used
to express any relationship between the robot's body and other element of 
its environment. The robot node is treated as a social agent, and can be build around
the following URI template : `agent:self`.

Robot node must be of
`rdf:type` `agent:social_agent`:

```turtle
{robot}
    rdf:type agent:social_agent;
```

## Objects

Objects are mental, but they can be observed during an [event](events).

```turtle
@prefix object qi:object# .
```

Objects can be associated to a phrase with a `rdfs:label` relationship.
They are of `rdf:type` `object:object`,
and may follow the following URI template: `object:object

```turtle
{object}
    rdf:type object:object;
    rdfs:label "cup"@en-us
```

## Locations

Everything happens in the namespace `location`:

```turtle
@prefix location qi:location# .
```

For now only a specific implementation of *locations* is supported,
the `location:transform_in_map`.
Any resource node pointing to a literal data,
the JSON form of a a `qi::actuation::Transform`.
The transform value is relative to a map,
just as the `location:transform_in_map` is `location:relative_to` a `location:map`.
A `location:map` is a resource pointing
to the binary literal data of a `qi::actuation::Map`.
Use the predicate `location:maps` to associate `location:map` to a `location:area`.
A `location:area` may have an `rdfs:label` and represents a mental object.
The general notion of *location* was not dealt with yet.

```turtle
{location}
    rdf:type location:transform_in_map
    location:relative_to {map}
{map}
    rdf:type location:map
    location:maps {area}
{area}
    rdf:type location:area
    rdfs:label "kitchen"@en-us
```

For areas, the recommended URI template is `location:area-{uid}`.
But for other types, there is no recommended URI template,
since it is expected data is returned when the URI is accessed,
and that the ontology does not provide this.

## Events

Events are occurrences of a fact.
It may be limited in space and time,
and offers a way to relate occurrences to each other.

For example, seeing an object at a given location is an event.
It is not intrinsic to the object,
because the object may move in time or space.
And on different occasions, the object may look different.
Hence, a picture of an object is rather
a property of the event when the object was seen,
than a property of the object.
Events may carry an arbitrary variety of information,
and may be derived into a variety of subtypes.

Generic event information should be namespaced by:

```turtle
@prefix event qi:event# .
```

The main event type is `event:event`,
and event instances should be named `event:event-{uid}`.
Events should be linked to one precise fact,
expressed by a single triple.
The event node is a reification of this triple.
See [W3C's reification specification](https://www.w3.org/TR/rdf-schema/#ch_reificationvocab).
Then, event can have `event:occurred_at` a certain time.

For example, if the robot performed some action:

```turtle
{event}
    rdf:type event:event
    rdf:subject agent:self
    rdf:predicate behavior:performs
    rdf:object {action}
    event:occurred_at "2012-12-21T21:21:21Z"^^xsd:datetime
```

Events may be responded to using the `event:responds_to` predicate,
usually by other events.
It involves a sort of logical causality, that can be used to describe
physical chains of events, but that carry an intentionality
that `behavior:consecutive_to` does not.

## Communication

We propose some summarized ontology to express communication acts
more concisely.
A communication act is an act (an event describing an action being performed)
involving an intent of communication from the performer to the addressee.

A communication act would be named `event:event-{uid}`,
just like any event, but would be of type `event:communication_act`
(as well as of type `event:event`, which is a parent type),
involve a performer, which should be a `{social_agent}`
used as an object of the `event:act_performed_by` predicate,
and an addressee, as another `{social_agent}`
used as an object of an `event:communication_addressee` predicate.

A communication act would typically be responded to in dialogues:

```turtle
{event_1}
    rdf:type event:communication_act
    event:act_performed_by {social_agent}
    event:addressee agent:self
{event_2}
    rdf:type event:communication_act
    event:act_performed_by agent:self
    event:communication_addressee {social_agent}
    event:responds_to {event_1}
```

## TODO

- ensure all code snippets are valid
- clear specification of Qi URIs
- behaviors with events reactions
- behaviors with conditional branches
- OWL file representing the constraints in this ontology,
and a tool to detect discrepancies, or even fix them.
- could it  be useful to add `rdfs:label` to behavior parameters so that to talk
about them?
- perceptual memory associated to events
