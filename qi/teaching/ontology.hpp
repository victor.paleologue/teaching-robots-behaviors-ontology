#ifndef TEACHING_ONTOLOGY_HPP
#define TEACHING_ONTOLOGY_HPP

#include <string>
#include <qi/knowledge/knowledge.hpp>

namespace qi
{
namespace teaching
{
// TODO: this file could be made easier to maintain:
// - by using macros, less would need to be written
// - with a formalized specification of the ontology, C++ code could be generated

// Namespaces
//============

/// RDF namespace.
const std::string& rdfNs();

/// RDF Schema namespace.
const std::string& rdfsNs();

/// Local knowledge namespace (qiknowledge://).
const std::string& localNs();

/// Current service directory namespace (qi://).
const std::string& qiNs();

/// Qi SDK API namespace (qisdk://).
const std::string& qiSdkNs();

/// Behavior namespace.
const std::string& behaviorNs();

/// Semantic namespace.
const std::string& semanticNs();

/// Agent namespace.
const std::string& agentNs();

/// Object namespace.
const std::string& objectNs();

/// Location namespace.
const std::string& locationNs();

/// Event namespace.
const std::string& eventNs();

// URI templates or prefixes
//===========================

/// URI of a method to call through a session.
std::string qiCallUri(const std::string& methodCallPattern);
// TODO: a variadic template that can generate the URI from typed data.

/// URI of an action in the Qi SDK.
std::string qiSdkActionUri(const std::string& actionName);

/// URI of an action's argument in the Qi SDK.
std::string qiSdkActionArgUri(const std::string& actionName, const std::string& argName);

/// URI prefix for behaviors.
const std::string& behaviorPrefix();

/// URI prefix for animations.
const std::string& animationPrefix();

/// URI prefix for compositions.
const std::string& compositionPrefix();

/// URI prefix for semantic frames.
const std::string& semanticFramePrefix();

/// URI prefix for social agents.
const std::string& socialAgentPrefix();

/// URI prefix for the robot.
const std::string& robotPrefix();

/// URI prefix for objects.
const std::string& objectPrefix();

/// URI prefix for areas.
const std::string& areaPrefix();

/// URI prefix for events.
const std::string& eventPrefix();

/// Generates resource with a fixed prefix.
/// It is guaranteed unique because an UUID is used.
/// @todo Move this to knowledge project.
knowledge::ResourceNode makeUniqueResource(const std::string& prefix);

// Usual predicates
//==================
/// Typing.
const std::string& rdfType();
const knowledge::ResourceNode& rdfTypeResource();

/// Subject in reification.
const std::string& rdfSubject();
const knowledge::ResourceNode& rdfSubjectResource();

/// Predicate in reification.
const std::string& rdfPredicate();
const knowledge::ResourceNode& rdfPredicateResource();

/// Object in reification.
const std::string& rdfObject();
const knowledge::ResourceNode& rdfObjectResource();

/// Natural language label.
const std::string& rdfsLabel();
const knowledge::ResourceNode& rdfsLabelResource();

/// Implementation relation.
const std::string& implementedBy();
const knowledge::ResourceNode& implementedByResource();

/// Expression relation.
const std::string& expressedBy();
const knowledge::ResourceNode& expressedByResource();

/// Composition relation.
const std::string& composedOf();
const knowledge::ResourceNode& composedOfResource();

/// Consecutivity relation.
const std::string& consecutiveTo();
const knowledge::ResourceNode& consecutiveToResource();

/// Colocalization relation.
const std::string& colocalizedWith();
const knowledge::ResourceNode& colocalizedWithResource();

/// Face picture associated with social agents (humans).
const std::string& facePicture();

/// Value relative to something (used for transforms in maps).
const std::string& relativeTo();
const knowledge::ResourceNode& relativeToResource();

/// Associates some map data to an area.
const std::string& maps();
const knowledge::ResourceNode& mapsResource();

/// Specifies a punctual time of occurrence for an event.
const std::string& occurredAt();
const knowledge::ResourceNode& occurredAtResource();

/// The performer of an act event.
const std::string& performedBy();
const knowledge::ResourceNode& performedByResource();

/// A target of a communication act.
const std::string& communicationAddressee();
const knowledge::ResourceNode& communicationAddresseeResource();

/// Tells that an event is a response to another one.
const std::string& respondsTo();
const knowledge::ResourceNode& respondsToResource();

// Taxonomy
//==========
const std::string& behaviorType();
const knowledge::ResourceNode& behaviorTypeResource();
const knowledge::Node& behaviorTypeNode();

const std::string& socialAgentType();
const knowledge::ResourceNode& socialAgentTypeResource();
const knowledge::Node& socialAgentTypeNode();

const std::string& objectType();
const knowledge::ResourceNode& objectTypeResource();
const knowledge::Node& objectTypeNode();

const std::string& transformInMapType();
const knowledge::ResourceNode& transformInMapTypeResource();
const knowledge::Node& transformInMapTypeNode();

const std::string& mapType();
const knowledge::ResourceNode& mapTypeResource();
const knowledge::Node& mapTypeNode();

const std::string& areaType();
const knowledge::ResourceNode& areaTypeResource();
const knowledge::Node& areaTypeNode();

const std::string& eventType();
const knowledge::ResourceNode& eventTypeResource();
const knowledge::Node& eventTypeNode();

const std::string& communicationActType();
const knowledge::ResourceNode& communicationActTypeResource();
const knowledge::Node& communicationActTypeNode();

// Entities
//=========
const std::string& agentSelf();
const knowledge::ResourceNode& agentSelfResource();
const knowledge::Node& agentSelfNode();

// Coffee machines
//=================
/// Make a triple from URIs.
knowledge::Triple makeTripleFromUris(
    const std::string& subject,
    const std::string& predicate,
    const std::string& object);

/// Make a triple from resources.
knowledge::Triple makeTripleFromResources(
    const knowledge::ResourceNode& subject,
    const knowledge::ResourceNode& predicate,
    const knowledge::ResourceNode& object);

} // teaching
} // qi

#endif // TEACHING_ONTOLOGY_HPP
