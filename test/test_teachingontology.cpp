#include <gmock/gmock.h>
#include <chrono>
#include <boost/filesystem.hpp>
#include <qi/anymodule.hpp>
#include <qi/application.hpp>
#include <qi/path.hpp>
#include <qi/teaching/ontology.hpp>
#include <qi/knowledge/knowledge.hpp>
#include <qi/knowledge/util.hpp>

qiLogCategory("Test.TeachingOntology");

using namespace qi::teaching;
using namespace qi::knowledge;

int main(int argc, char** argv) {
  qi::Application app{argc, argv};
  ::testing::InitGoogleTest(&argc, argv);
  return RUN_ALL_TESTS();
}

// Fixture
class TeachingOntology: public ::testing::Test
{
protected:
  TeachingOntology();
  ~TeachingOntology();

  qi::Path _knowledgeDir;
  EditableKnowledgeBasePtr _knowledge;
  EditableKnowledgeGraphPtr _beliefs;
};


TeachingOntology::TeachingOntology()
  : _knowledgeDir(qi::os::mktmpdir("KnowledgeTest"))
  , _knowledge(qi::import("knowledge_module").call<EditableKnowledgeBasePtr>(
                 "EditableKnowledgeBase", _knowledgeDir.str())) {}

TeachingOntology::~TeachingOntology() {
  _knowledge.reset();
  boost::filesystem::remove_all(_knowledgeDir.bfsPath());
}


TEST_F(TeachingOntology, MakeManyUniqueNodes) {
  auto graph = _knowledge->editableKnowledgeGraph("whatever");
  static const auto typeResource = makeResourceNodeFromUrl(rdfType());
  static const auto behaviorTypeResource = makeResourceNodeFromUrl(behaviorType());
  static const auto behaviorTypeNode = makeNodeFromResourceNode(behaviorTypeResource);

  const auto start = std::chrono::steady_clock::now();
  const auto end = start + std::chrono::seconds{5};
  std::chrono::steady_clock::time_point now;

  auto nofNodesAdded = 0;
  do {
    auto behaviorResource = makeUniqueResource(behaviorPrefix());
    graph->add(Triple{behaviorResource, typeResource, behaviorTypeNode});
    qiLogInfo() << "Added node #" << ++nofNodesAdded << " (" << behaviorResource.url << ")";
    now = std::chrono::steady_clock::now();
  } while (now < end);
}

