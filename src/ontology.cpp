#include <qi/teaching/ontology.hpp>
#include <boost/uuid/random_generator.hpp>
#include <boost/uuid/uuid_io.hpp>
#include <qi/knowledge/util.hpp>

using namespace qi::knowledge;

namespace qi
{
namespace teaching
{

// Namespaces
//============

const std::string& rdfNs() {
  static const std::string res = "http://www.w3.org/1999/02/22-rdf-syntax-ns#";
  return res;
}

const std::string& rdfsNs() {
  static const std::string res = "http://www.w3.org/2000/01/rdf-schema#";
  return res;
}

const std::string& localNs() {
  static const std::string res = "qiknowledge://";
  return res;
}

const std::string& qiNs() {
  static const std::string res = "qi://";
  return res;
}

const std::string& qiSdkNs() {
  static const std::string res = "qisdk://";
  return res;
}

const std::string& behaviorNs() {
  static const std::string res = localNs() + "behavior#";
  return res;
}

const std::string& semanticNs() {
  static const std::string res = localNs() + "semantic#";
  return res;
}

const std::string& agentNs() {
  static const std::string res = localNs() + "agent#";
  return res;
}

const std::string& objectNs() {
  static const std::string res = localNs() + "object#";
  return res;
}

const std::string& locationNs() {
  static const std::string res = localNs() + "location#";
  return res;
}

const std::string& eventNs() {
  static const std::string res = localNs() + "event#";
  return res;
}

// URI templates or prefixes
//===========================

std::string qiCallUri(const std::string& methodCallPattern) {
  return qiNs() + methodCallPattern;
}

std::string qiSdkActionUri(const std::string& actionName) {
  return qiSdkNs() + "action/" + actionName;
}

std::string qiSdkActionArgUri(const std::string& actionName, const std::string& argName) {
  return qiSdkActionUri(actionName) + "#" + argName;
}

const std::string& behaviorPrefix() {
  static const std::string res = behaviorNs() + "behavior-";
  return res;
}

const std::string& animationPrefix() {
  static const std::string res = behaviorNs() + "animation-";
  return res;
}

const std::string& compositionPrefix() {
  static const std::string res = behaviorNs() + "composition-";
  return res;
}

const std::string& semanticFramePrefix() {
  static const std::string res = semanticNs() + "frame-";
  return res;
}

const std::string& socialAgentPrefix() {
  static const std::string res = agentNs() + "social_agent-";
  return res;
}

const std::string& robotPrefix() {
  static const std::string res = agentNs() + "self";
  return res;
}

const std::string& objectPrefix() {
  static const std::string res = objectNs() + "object-";
  return res;
}

const std::string& areaPrefix() {
  static const std::string res = locationNs() + "area-";
  return res;
}

const std::string& eventPrefix() {
  static const std::string res = eventNs() + "event-";
  return res;
}

ResourceNode makeUniqueResource(const std::string& prefix) {
  static boost::uuids::random_generator genUuid;
  ResourceNode resource;
  resource.url = prefix + boost::uuids::to_string(genUuid()).substr(0, 8);
  return resource;
}

// Usual predicates
//==================

const std::string& rdfType() {
  static const std::string res = rdfNs() + "type";
  return res;
}

const ResourceNode& rdfTypeResource() {
  static const auto res = makeResourceNodeFromUrl(rdfType());
  return res;
}

/// Subject in reification.
const std::string& rdfSubject() {
  static const std::string res = rdfNs() + "subject";
  return res;
}

const knowledge::ResourceNode& rdfSubjectResource() {
  static const auto res = makeResourceNodeFromUrl(rdfSubject());
  return res;
}

/// Predicate in reification.
const std::string& rdfPredicate() {
  static const std::string res = rdfNs() + "predicate";
  return res;
}

const knowledge::ResourceNode& rdfPredicateResource() {
  static const auto res = makeResourceNodeFromUrl(rdfPredicate());
  return res;
}

/// Object in reification.
const std::string& rdfObject() {
  static const std::string res = rdfNs() + "object";
  return res;
}

const knowledge::ResourceNode& rdfObjectResource() {
  static const auto res = makeResourceNodeFromUrl(rdfObject());
  return res;
}

const std::string& rdfsLabel() {
  static const std::string res = rdfsNs() + "label";
  return res;
}

const ResourceNode& rdfsLabelResource() {
  static const auto res = makeResourceNodeFromUrl(rdfsLabel());
  return res;
}

const std::string& implementedBy() {
  static const std::string res = behaviorNs() + "implemented_by";
  return res;
}

const ResourceNode& implementedByResource() {
  static const auto res = makeResourceNodeFromUrl(implementedBy());
  return res;
}

const std::string& expressedBy() {
  static const std::string res = behaviorNs() + "expressed_by";
  return res;
}

const ResourceNode& expressedByResource() {
  static const auto res = makeResourceNodeFromUrl(expressedBy());
  return res;
}

const std::string& composedOf() {
  static const std::string res = behaviorNs() + "composed_of";
  return res;
}

const ResourceNode& composedOfResource() {
  static const auto res = makeResourceNodeFromUrl(composedOf());
  return res;
}

const std::string& consecutiveTo() {
  static const std::string res = behaviorNs() + "consecutive_to";
  return res;
}

const ResourceNode& consecutiveToResource() {
  static const auto res = makeResourceNodeFromUrl(consecutiveTo());
  return res;
}

const std::string& colocalizedWith() {
  static const std::string res = agentNs() + "colocalized_with";
  return res;
}

const ResourceNode& colocalizedWithResource() {
  static const auto res = makeResourceNodeFromUrl(colocalizedWith());
  return res;
}

const std::string& facePicture() {
  static const std::string res = agentNs() + "face_picture";
  return res;
}

const std::string& relativeTo()
{
  static const std::string res = locationNs() + "relative_to";
  return res;
}

const std::string& maps() {
  static const std::string res = locationNs() + "maps";
  return res;
}

const knowledge::ResourceNode& mapsResource() {
  static const auto res = makeResourceNodeFromUrl(maps());
  return res;
}

const std::string& occurredAt() {
  static const std::string res = eventNs() + "occurred_at";
  return res;
}

const knowledge::ResourceNode& occurredAtResource() {
  static const auto res = makeResourceNodeFromUrl(occurredAt());
  return res;
}

const std::string& performedBy() {
  static const std::string res = eventNs() + "performed_by";
  return res;
}

const knowledge::ResourceNode& performedByResource() {
  static const auto res = makeResourceNodeFromUrl(performedBy());
  return res;
}

const std::string& communicationAddressee() {
  static const std::string res = eventNs() + "communication_addressee";
  return res;
}

const knowledge::ResourceNode& communicationAddresseeResource() {
  static const auto res = makeResourceNodeFromUrl(communicationAddressee());
  return res;
}

const std::string& respondsTo() {
  static const std::string res = eventNs() + "responds_to";
  return res;
}

const knowledge::ResourceNode& respondsToResource() {
  static const auto res = makeResourceNodeFromUrl(respondsTo());
  return res;
}

// Taxonomy
//==========

const std::string& behaviorType() {
  static const std::string res = behaviorNs() + "behavior";
  return res;
}

const ResourceNode& behaviorTypeResource() {
  static const auto res = makeResourceNodeFromUrl(behaviorType());
  return res;
}

const Node& behaviorTypeNode() {
  static const auto res = makeNodeFromResourceNode(behaviorTypeResource());
  return res;
}

const std::string& socialAgentType() {
  static const std::string res = agentNs() + "social_agent";
  return res;
}

const ResourceNode& socialAgentTypeResource() {
  static const auto res = makeResourceNodeFromUrl(socialAgentType());
  return res;
}

const Node& socialAgentTypeNode() {
  static const auto res = makeNodeFromResourceNode(socialAgentTypeResource());
  return res;
}

const std::string& objectType() {
  static const std::string res = objectNs() + "object";
  return res;
}

const knowledge::ResourceNode& objectTypeResource() {
  static const auto res = makeResourceNodeFromUrl(objectType());
  return res;
}

const knowledge::Node& objectTypeNode() {
  static const auto res = makeNodeFromResourceNode(objectTypeResource());
  return res;
}

const std::string& transformInMapType() {
  static const std::string res = locationNs() + "transform_in_map";
  return res;
}

const knowledge::ResourceNode& transformInMapTypeResource() {
  static const auto res = makeResourceNodeFromUrl(transformInMapType());
  return res;
}

const knowledge::Node& transformInMapTypeNode() {
  static const auto res = makeNodeFromResourceNode(transformInMapTypeResource());
  return res;
}

const std::string& mapType() {
  static const std::string res = locationNs() + "map";
  return res;
}

const knowledge::ResourceNode& mapTypeResource() {
  static const auto res = makeResourceNodeFromUrl(mapType());
  return res;
}

const knowledge::Node& mapTypeNode() {
  static const auto res = makeNodeFromResourceNode(mapTypeResource());
  return res;
}

const std::string& areaType() {
  static const std::string res = locationNs() + "area";
  return res;
}

const knowledge::ResourceNode& areaTypeResource() {
  static const auto res = makeResourceNodeFromUrl(areaType());
  return res;
}

const knowledge::Node& areaTypeNode() {
  static const auto res = makeNodeFromResourceNode(areaTypeResource());
  return res;
}

const std::string& eventType() {
  static const std::string res = eventNs() + "event";
  return res;
}

const knowledge::ResourceNode& eventTypeResource() {
  static const auto res = makeResourceNodeFromUrl(eventType());
  return res;
}

const knowledge::Node& eventTypeNode() {
  static const auto res = makeNodeFromResourceNode(eventTypeResource());
  return res;
}

const std::string& communicationActType() {
  static const std::string res = eventNs() + "communication_act";
  return res;
}

const knowledge::ResourceNode& communicationActTypeResource() {
  static const auto res = makeResourceNodeFromUrl(communicationActType());
  return res;
}

const knowledge::Node& communicationActTypeNode() {
  static const auto res = makeNodeFromResourceNode(communicationActTypeResource());
  return res;
}

// Entities
//=========
const std::string& agentSelf() {
  static const std::string res = agentNs() + "self";
  return res;
}

const knowledge::ResourceNode& agentSelfResource() {
  static const auto res = makeResourceNodeFromUrl(agentSelf());
  return res;
}

const knowledge::Node& agentSelfNode() {
  static const auto res = makeNodeFromResourceNode(agentSelfResource());
  return res;
}

// Coffee machines
//=================

Triple makeTripleFromUris(
    const std::string& subject,
    const std::string& predicate,
    const std::string& object) {
  return Triple{
    ResourceNode{subject},
    ResourceNode{predicate},
    makeNodeFromResourceNode(ResourceNode{object})
  };
}

knowledge::Triple makeTripleFromResources(
    const knowledge::ResourceNode& subject,
    const knowledge::ResourceNode& predicate,
    const knowledge::ResourceNode& object) {
  return Triple{subject, predicate, makeNodeFromResourceNode(object)};
}
} // teaching
} // qi
